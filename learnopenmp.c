/*
 ** Code to implement a d2q9-bgk lattice boltzmann scheme.
 ** 'd2' inidates a 2-dimensional grid, and
 ** 'q5' indicates 5 velocities per grid cell.
 ** 'bgk' refers to the Bhatnagar-Gross-Krook collision step.
 **
 ** The 'speeds' in each cell are numbered as follows:
 **
 **   2
 **   |
 ** 3-0-1
 **   |
 **   4
 **
 ** A 2D grid:
 **
 **           cols
 **       --- --- ---
 **      | D | E | F |
 ** rows  --- --- ---
 **      | A | B | C |
 **       --- --- ---
 **
 ** 'unwrapped' in row major order to give a 1D array:
 **
 **  --- --- --- --- --- ---
 ** | A | B | C | D | E | F |
 **  --- --- --- --- --- ---
 **
 ** Grid indicies are:
 **
 **          ny
 **          ^
 **          |       cols(jj)
 |  ----- ----- -----
 **          | | ... | ... | etc |
 **          |  ----- ----- -----
 ** rows(ii) | | 1,0 | 1,1 | 1,2 |
 **          |  ----- ----- -----
 **          | | 0,0 | 0,1 | 0,2 |
 **          |  ----- ----- -----
 **          ---------------------> nx
 */

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<sys/time.h>
#include<sys/resource.h>
#include <limits.h>		/* for CHAR_BIT */
#include <omp.h>
#include <xmmintrin.h>
#define MASK(b) (1 << ((b) % CHAR_BIT))
#define INSERT(a, b) ((a)[BITSPERCHAR(b)] |= MASK(b))
#define ISBITSET(a, b) ((a)[BITSPERCHAR(b)] & MASK(b))
#define BITSPERCHAR(b) ((b) / CHAR_BIT)
#define INITBIT(a, b) ((a)[BITSPERCHAR(b)] &= ~MASK(b))
#define BITARRAY(nb) ((nb + CHAR_BIT - 1) / CHAR_BIT)

#define NSPEEDS         4
#define PARAMFILE       "input.params"
#define OBSTACLEFILE    "obstacles.dat"
#define FINALSTATEFILE  "final_state.dat"
#define AVVELSFILE      "av_vels.dat"
float by_calc_c_sq =  3.0; /* square of speed of sound */
float by_calc_twice_c_sq = 1.5; /* square of speed of sound */
float by_calc_twice_sq_c_sq =  4.5;
float calc_w0 = 1.0 / 3.0; /* weighting factor */
float calc_w1 = 1.0 / 6.0; /* weighting factor */

#define ALIGN 64

/* struct to hold the parameter values */
typedef struct {
	int nx; /* no. of cells in x-direction */
	int ny; /* no. of cells in y-direction */
	int maxIters; /* no. of iterations */
	int reynolds_dim; /* dimension for Reynolds number */
	float density; /* density per link */
	float accel; /* density redistribution */
	float omega; /* relaxation parameter */
	float weighting_factor;
} t_param;

/* struct to hold the 'speed' values */
typedef struct {
	float speeds[NSPEEDS];
	//float dummy[3];
} t_speed;

enum boolean {
	FALSE, TRUE
};

/*
 ** function prototypes
 */

/* load params, allocate memory, load obstacles & initialise fluid particle densities */
int initialise(t_param* params, t_speed** cells_ptr, t_speed** tmp_cells_ptr,
		char** obstacles_ptr, float** cells_0);
int together(const t_param params, t_speed** cells, t_speed** tmp_cells,
		const char* obstacles,FILE* av_vels_file_ptr,float* cells_0);
/* 
 ** The main calculation methods.
 ** timestep calls, in order, the functions:
 ** accelerate_flow(), propagate(), rebound() & collision()
 */
static int calc(float speed1, float speed2, float speed3, float speed4,
		float local_density, float* d_equ);
//int timestep(const t_param params, t_speed* cells, t_speed* tmp_cells,
		//const int* obstacles,float* vel);
//int accelerate_flow(const t_param params, t_speed* cells, const int* obstacles);
//int propagate(const t_param params, t_speed* cells, t_speed* tmp_cells);
//int rebound(const t_param params, t_speed* cells, t_speed* tmp_cells,
	//	const int* obstacles);
//int collision(const t_param params, t_speed* cells, t_speed* tmp_cells,
	//	const int* obstacles,float* vel);
int write_values(const t_param params, t_speed* cells, const char* obstacles,
		float* cells_0);

/* finalise, including freeing up allocated memory */
int finalise(const t_param* params, t_speed** cells_ptr,
		t_speed** tmp_cells_ptr, char** obstacles_ptr, FILE* av_vels_file_ptr,float** cells_0);

/* Sum all the densities in the grid.
 ** The total should remain constant from one timestep to the next. */
float total_density(const t_param params, t_speed* cells,float* cells_0);

/* compute average velocity */
float av_velocity(const t_param params, t_speed* cells, const char* obstacles,float* cells_0);

/* calculate Reynolds number */
float calc_reynolds(const t_param params, t_speed* cells, const char* obstacles,float* cells_0);

/* utility functions */
void die(const char* message, const int line, const char *file);

/*
 ** main program:
 ** initialise, timestep loop, finalise
 */
int main(int argc, char* argv[]) {
	t_param params; /* struct to hold parameter values */
	t_speed* cells = NULL; /* grid containing fluid densities */
	float* cells_0 = NULL;
	t_speed* tmp_cells = NULL; /* scratch space */
	//int* tmp_scratch = NULL;
	char* obstacles = NULL; /* grid indicating which cells are blocked */
	//float* av_vels = NULL; /* a record of the av. velocity computed for each timestep */
	//int ii; /* generic counter */
	struct timeval timstr; /* structure to hold elapsed time */
	struct rusage ru; /* structure to hold CPU time--system and user */
	double tic, toc; /* floating point numbers to calculate elapsed wallclock time */
	double usrtim; /* floating point number to record elapsed user CPU time */
	double systim; /* floating point number to record elapsed system CPU time */
	FILE* av_vels_file_ptr;
	//int nthreads;
    //#pragma omp parallel
    //{
        //#pragma omp single
        //{
            //omp_set_num_threads(8);
        //}
    //}
	//omp_set_num_threads(4);
	//int threads = 0;
	//threads = omp_get_num_threads();
	//printf("%d\n",nthreads);
	/* initialise our data structures and load values from file */
	initialise(&params, &cells, &tmp_cells, &obstacles, &cells_0);

	/* iterate for maxIters timesteps */
	gettimeofday(&timstr, NULL);
	tic = timstr.tv_sec + (timstr.tv_usec / 1000000.0);
	//float d;
	av_vels_file_ptr = fopen(AVVELSFILE, "w");
	if (av_vels_file_ptr == NULL) {
		die("could not open file output file", __LINE__, __FILE__);
	}

	//for (ii = 0; ii < params.maxIters; ii++) {
	together(params, &cells, &tmp_cells, obstacles,av_vels_file_ptr,cells_0);
		//collision(params, cells, tmp_cells, obstacles,&d);
		//timestep(params, cells, tmp_cells, obstacles,&d);
		//av_vels[ii] = d;
		//av_vels[ii] = av_velocity(params, cells, obstacles);
#ifdef DEBUG
		//printf("==timestep: %d==\n",ii);
		//printf("av velocity: %.12E\n", av_vels[ii]);
		//printf("tot density: %.12E\n",total_density(params,cells));
#endif
	//}
	gettimeofday(&timstr, NULL);
	toc = timstr.tv_sec + (timstr.tv_usec / 1000000.0);
	getrusage(RUSAGE_SELF, &ru);
	timstr = ru.ru_utime;
	usrtim = timstr.tv_sec + (timstr.tv_usec / 1000000.0);
	timstr = ru.ru_stime;
	systim = timstr.tv_sec + (timstr.tv_usec / 1000000.0);

	/* write final values and free memory */
	printf("==done==\n");
	printf("Reynolds number:\t\t%.12E\n",
			calc_reynolds(params, cells, obstacles,cells_0));
	printf("Elapsed time:\t\t\t%.6lf (s)\n", toc - tic);
	printf("Elapsed user CPU time:\t\t%.6lf (s)\n", usrtim);
	printf("Elapsed system CPU time:\t%.6lf (s)\n", systim);
	write_values(params, cells, obstacles, cells_0);
	finalise(&params, &cells, &tmp_cells, &obstacles, av_vels_file_ptr,&cells_0);

	return EXIT_SUCCESS;
}

void *aligned_malloc(int size) {
    void *mem = malloc(size+ALIGN+sizeof(void*));
    void **ptr = (void**)((long)(mem+ALIGN+sizeof(void*)) & ~(ALIGN-1));
    ptr[-1] = mem;
    return ptr;
}

void aligned_free(void *ptr) {
    free(((void**)ptr)[-1]);
}

int together(const t_param params, t_speed** cells_ptr, t_speed** tmp_cells_ptr,
		const char* obstacles,FILE* av_vels_file_ptr,float* cells_0) {
	int ii,kk,jj,cellIndex; /* generic counters */
	//const int nynx = params.ny*params.nx;
	t_speed* cells = *cells_ptr;
	t_speed* tmp_cells = *tmp_cells_ptr;
	//const int ny = params.ny;
	//const int nx = params.nx;
	//const int ny_1nx = (ny - 1) * nx;
	//const int nynx = ny * nx;
	//const int nx_1 = nx - 1;
	//int jj = 0;

	float tot_u_x = 0; /* accumulated x-components of velocity */
	int tot_cells = 0; /* no. of cells used in calculation */
	const int ny = params.ny;
	const int nx = params.nx;
	const int ny_1nx = (ny - 1) * nx;
	//const int nynx = ny * nx;
	const int nx_1 = nx - 1;
	//double tmp_vec[NSPEEDS];
	//double omega = params.omega;
	float d_equ[NSPEEDS+1]; /* equilibrium densities */
	float local_density; /* sum of densities in a particular cell */
	float tmp[NSPEEDS] ;
	//int myii,myjj;


#pragma omp parallel default(none) shared(cells_ptr,tmp_cells_ptr,cells,obstacles,tmp_cells,tot_u_x,tot_cells,av_vels_file_ptr,cells_0)
	{
//#pragma omp for schedule(dynamic,50) private(ii,cellIndex)
	/*for (ii = 0; ii < params.ny; ii++) {
		cellIndex = ii * params.nx;
		if (!obstacles[cellIndex]
				&& (cells[cellIndex].speeds[3] - params.weighting_factor) > 0.0) {
			cells[cellIndex].speeds[1] += params.weighting_factor;
			cells[cellIndex].speeds[3] -= params.weighting_factor;
		}
	}*/
		int iter;

		for (iter = 0; iter < params.maxIters; iter++) {

		//int x_e, x_w, y_n, y_s;

		//int jj=0;
#pragma omp for private(ii,cellIndex)
		for (ii = 0; ii < params.ny; ii++) {
			cellIndex = ii * params.nx;
			if (! ISBITSET(obstacles, cellIndex)
					&& (cells[cellIndex].speeds[2] - params.weighting_factor) > 0.0) {
				cells[cellIndex].speeds[0] += params.weighting_factor;
				cells[cellIndex].speeds[2] -= params.weighting_factor;
			}

			//for (ii = 0; ii < params.ny; ii++) {
				//cellIndex = ii * params.nx;

			//for (jj = 0; jj < params.nx; jj++) {
				//y_n = (ii + 1) % params.ny;
				//x_e = (jj + 1) % params.nx;
				//y_s = (ii == 0) ? (ii + params.ny - 1) : (ii - 1);
				//x_w = (jj == 0) ? (jj + params.nx - 1) : (jj - 1);
				//tmp_cells[ii * params.nx + x_e].speeds[0] = cells[cellIndex + jj].speeds[0];
				//tmp_cells[y_n * params.nx + jj].speeds[1] = cells[cellIndex + jj].speeds[1];
				//tmp_cells[ii * params.nx + x_w].speeds[2] = cells[cellIndex + jj].speeds[2];
				//tmp_cells[y_s * params.nx + jj].speeds[3] = cells[cellIndex + jj].speeds[3];

			//}
			/*if((kk % (nx - 1)) == 0){
					tmp_cells[kk].speeds[2] = cells[kk - nx_1].speeds[2];
				}else{
					tmp_cells[kk].speeds[2] = cells[kk + 1].speeds[2];
				}

				if ((kk % nx) == 0) {
					tmp_cells[kk].speeds[0] = cells[kk + nx_1].speeds[0];
				}else{
					tmp_cells[kk].speeds[0] = cells[kk - 1].speeds[0];
				}
				if(kk < nx){
					tmp_cells[kk].speeds[1] = cells[ny_1nx + kk].speeds[1];
				}else{
					tmp_cells[kk].speeds[1] = cells[kk - nx].speeds[1];
				}
				if(kk < ny_1nx){
					tmp_cells[kk].speeds[3] = cells[kk + nx].speeds[3];
				}else{
					tmp_cells[kk].speeds[3] = cells[kk-ny_1nx].speeds[3];
				}*/
			//}

		}



	/* loop over the cells in the grid */
//#pragma omp for private(ii)
	//for (ii = 0; ii < nynx; ii++) {
		//for (jj = 0; jj < params.nx; jj++) {
			/* if the cell contains an obstacle */
			//if (ISBITSET(obstacles,ii)) {
				/* called after propagate, so taking values from scratch space
				 ** mirroring, and writing into main grid */
				//cells[ii].speeds[1] = tmp_cells[ii].speeds[3];
				//cells[ii].speeds[2] = tmp_cells[ii].speeds[4];
				//cells[ii].speeds[3] = tmp_cells[ii].speeds[1];
				//cells[ii].speeds[4] = tmp_cells[ii].speeds[2];
			//}
		//}
	//}

	/*for (ii = 0; ii < nynx; ii++) { // All
		tmp_cells[ii].speeds[0] = cells[ii].speeds[0];
		if(ii % (nx - 1) == 0){
			tmp_cells[ii].speeds[3] = cells[ii - nx_1].speeds[3];
		}else{
			tmp_cells[ii].speeds[3] = cells[ii + 1].speeds[3];
		}

		if (ii % nx ) {
				tmp_cells[ii].speeds[1] = cells[ii + nx_1].speeds[1];
		}else{
			tmp_cells[ii].speeds[1] = cells[ii - 1].speeds[1];
		}
		if(ii < nx){
			tmp_cells[ii].speeds[2] = cells[ny_1nx + ii].speeds[2];
		}else{
			tmp_cells[ii].speeds[2] = cells[ii - nx].speeds[2];
		}
		if(ii < ny_1nx){
			tmp_cells[ii].speeds[4] = cells[ii + nx].speeds[4];
		}else{
			tmp_cells[ii].speeds[4] = cells[jj].speeds[4];
			jj++;
		}
	}
	*/


#pragma omp for private(ii,jj,kk,d_equ,local_density,tmp,cellIndex) reduction(+: tot_u_x,tot_cells)
	for (ii = 0; ii < ny; ii++) {
		for(jj=0 ; jj < nx ; jj++){
			cellIndex = ii * params.nx + jj;
		//myii = ii/nx;
		//myjj = ii - myii * nx;


		if(jj == nx-1){
			tmp[2] = cells[cellIndex - nx_1].speeds[2];
			//if ( ((ii - nx_1) / nx)*nx==(ii - nx_1) )
				//if( !ISBITSET(obstacles, ii - nx_1)
					//		&& (cells[ii - nx_1].speeds[2] - params.weighting_factor) > 0.0)
						//tmp[2] -= params.weighting_factor;
		}else{
			tmp[2] = cells[cellIndex + 1].speeds[2];
			//if ( ((ii + 1) / nx)*nx==(ii + 1))
				//if( !ISBITSET(obstacles, ii + 1)
					//		&& (cells[ii + 1 ].speeds[2] - params.weighting_factor) > 0.0)
						//tmp[2] -= params.weighting_factor;
		}

		if (jj == 0) {
			tmp[0] = cells[cellIndex + nx_1].speeds[0];
			//if ( ((ii + nx_1) / nx)*nx==(ii + nx_1) )
				//if( !ISBITSET(obstacles, ii + nx_1)
					//		&& (cells[ii + nx_1].speeds[2] - params.weighting_factor) > 0.0)
						//tmp[0] += params.weighting_factor;
		}else{
			tmp[0] = cells[cellIndex - 1].speeds[0];
			//if ( ((ii - 1) / nx)*nx==(ii - 1) )
				//if( !ISBITSET(obstacles, ii - 1)
					//		&& (cells[ii - 1].speeds[2] - params.weighting_factor) > 0.0)
						//tmp[0] += params.weighting_factor;
		}

		if(cellIndex < nx){
			tmp[1] = cells[ny_1nx + cellIndex].speeds[1];
		}else{
			tmp[1] = cells[cellIndex - nx].speeds[1];
		}

		if(cellIndex < ny_1nx){
			tmp[3] = cells[cellIndex + nx].speeds[3];
		}else{
			tmp[3] = cells[cellIndex-ny_1nx].speeds[3];
		}



		if (!ISBITSET(obstacles,cellIndex)) {
			local_density = 0.0;
			for (kk = 0; kk < NSPEEDS; kk+=4) {
				local_density = tmp[kk+0] + tmp[kk+1] + tmp[kk+2] + tmp[kk+3];
			}
			local_density += cells_0[cellIndex];

			calc(tmp[0], tmp[1],
					tmp[2], tmp[3],
					local_density, d_equ);

			local_density = 0.0;
			//for (kk = 0; kk < NSPEEDS; kk++) {
				//tmp_cells[cellIndex].speeds[kk] = tmp[kk] + params.omega * (d_equ[kk] - tmp[kk]);
			//}

			//__m128 SSEminus    = _mm_set1_pd(-1.0);
			for(kk = 0; kk < NSPEEDS; kk+=4){
				__m128 SSEomega=_mm_set1_ps(params.omega);
				__m128 SSEd_equ=_mm_loadu_ps(&d_equ[kk]);
				__m128 SSEtmp=_mm_loadu_ps(&tmp[kk]);
				//__m128 SSEtmpminus = _mm_sub_ps(SSEd_equ,SSEtmp);
				//__m128 SSEdquminustmpomega = _mm_mul_ps(SSEomega,SSEtmpminus);
				//__m128 v=_mm_load1_ps(&tmp_cells[cellIndex].speeds[kk]);
				__m128 v =_mm_add_ps(SSEtmp,_mm_mul_ps(SSEomega,_mm_sub_ps(SSEd_equ,SSEtmp)));
				_mm_storeu_ps(&tmp_cells[cellIndex].speeds[kk],v);
			}

			for (kk = 0; kk < NSPEEDS; kk+=4) {
				local_density = cells[cellIndex].speeds[kk+0] + cells[cellIndex].speeds[kk+1] + cells[cellIndex].speeds[kk+2] + cells[cellIndex].speeds[kk+3];
			}

			cells_0[cellIndex] = (cells_0[cellIndex] + params.omega * ( d_equ[4] - cells_0[cellIndex]));
			local_density+= cells_0[cellIndex];

			tot_u_x += (cells[cellIndex].speeds[0] - cells[cellIndex].speeds[2]) / local_density;
			tot_cells+=1;
		}else{
			//if (ISBITSET(obstacles,ii)) {
				/* called after propagate, so taking values from scratch space
				 ** mirroring, and writing into main grid */
				tmp_cells[cellIndex].speeds[0] = tmp[2];
				tmp_cells[cellIndex].speeds[1] = tmp[3];
				tmp_cells[cellIndex].speeds[2] = tmp[0];
				tmp_cells[cellIndex].speeds[3] = tmp[1];
			//}

		}
		}
		//}
	}
#pragma omp single
	{

		fprintf(av_vels_file_ptr, "%d:\t%.6f\n", iter, tot_u_x / (float) tot_cells);
		//av_vels[iter] = tot_u_x / (float) tot_cells;
		#ifdef DEBUG
				printf("==timestep: %d==\n",ii);
				printf("av velocity: %.6f\n", tot_u_x / (float) tot_cells);
				printf("tot density: %.6f\n",total_density(params,cells,cells_0));
		#endif

				t_speed** tmp = cells_ptr;
				cells_ptr = tmp_cells_ptr;
				tmp_cells_ptr = tmp;
				cells = *cells_ptr;
				tmp_cells = *tmp_cells_ptr;

	}
	}

	}
	//*vel= tot_u_x / (float) tot_cells;
	return EXIT_SUCCESS;
}

static int calc(float speed1, float speed2, float speed3, float speed4,
		float local_density, float* d_equ) {
	//float u_x, u_y;
	//float u[2];
	//float u_sq;

	float u_x  = (speed1 - speed3) / local_density;
	float u_y = (speed2 - speed4) / local_density;

	float u_x_sq = u_x * u_x;
	float u_y_sq = u_y * u_y;
	float u_sq = u_x_sq + u_y_sq;
	//u[0] = u_x;
	//u[1] = u_y;
	//u[2] = -u_x;
	//u[3] = -u_y;
	float utimescsq = u_sq * by_calc_twice_c_sq;
	float calctimeslocaldensity = calc_w1 * local_density;
	float ux_twicw_calc = (u_x_sq) * by_calc_twice_sq_c_sq;
	float uy_twicw_calc = (u_y_sq) * by_calc_twice_sq_c_sq;
	float diffx = ux_twicw_calc - utimescsq;
	float diffy = uy_twicw_calc - utimescsq;
	float oneplus_diffx = 1.0 + diffx;
	float oneplus_diffy = 1.0 + diffy;
	float calctimesdiffx = calctimeslocaldensity * oneplus_diffx;
	float calctimesdiffy = calctimeslocaldensity * oneplus_diffy;
	float calctimesbycals = calctimeslocaldensity * by_calc_c_sq;
	float calcu0 = calctimesbycals * u_x;
	float calcu1 = calctimesbycals * u_y;
	d_equ[0] = calctimesdiffx + calcu0 ;
	d_equ[1] = calctimesdiffy + calcu1 ;
	d_equ[2] = calctimesdiffx - calcu0;
	d_equ[3] = calctimesdiffy - calcu1 ;
	d_equ[4] = calc_w0 * local_density * (1.0 - utimescsq);

	//int i = 0;
	//for(i=0;i<NSPEEDS;i+=4){
		//__m128 SSEa=_mm_load1_ps(&by_calc_c_sq);
		//__m128 SSEb=_mm_load1_ps(&oneplus_diffy);
		//__m128 SSEc=_mm_load1_ps(&calctimeslocaldensity);
		//__m128 d_equ=_mm_load_ps(&d_equ[i+1]);
		//__m128 u=_mm_load_ps(&u[i]);
		//d_equ=_mm_mul_ps(SSEc,_mm_add_ps(_mm_mul_ps(u,SSEa),SSEb));
		//_mm_store_ps(&d_equ[i+1],d_equ);
	//}
	return EXIT_SUCCESS;
}

int initialise(t_param* params, t_speed** cells_ptr, t_speed** tmp_cells_ptr,
		char** obstacles_ptr, float** cells_0) {
	FILE *fp; /* file pointer */
	int ii, jj; /* generic counters */
	int xx, yy; /* generic array indices */
	int blocked; /* indicates whether a cell is blocked by an obstacle */
	int retval; /* to hold return value for checking */
	float w0, w1; /* weighting factors */

	/* open the parameter file */
	fp = fopen(PARAMFILE, "r");
	if (fp == NULL) {
		die("could not open file input.params", __LINE__, __FILE__);
	}

	/* read in the parameter values */
	retval = fscanf(fp, "%d\n", &(params->nx));
	if (retval != 1)
		die("could not read param file: nx", __LINE__, __FILE__);
	retval = fscanf(fp, "%d\n", &(params->ny));
	if (retval != 1)
		die("could not read param file: ny", __LINE__, __FILE__);
	retval = fscanf(fp, "%d\n", &(params->maxIters));
	if (retval != 1)
		die("could not read param file: maxIters", __LINE__, __FILE__);
	retval = fscanf(fp, "%d\n", &(params->reynolds_dim));
	if (retval != 1)
		die("could not read param file: reynolds_dim", __LINE__, __FILE__);
	retval = fscanf(fp, "%f\n", &(params->density));
	if (retval != 1)
		die("could not read param file: density", __LINE__, __FILE__);
	retval = fscanf(fp, "%f\n", &(params->accel));
	if (retval != 1)
		die("could not read param file: accel", __LINE__, __FILE__);
	retval = fscanf(fp, "%f\n", &(params->omega));
	if (retval != 1)
		die("could not read param file: omega", __LINE__, __FILE__);
	params->weighting_factor =  params->density * params->accel / 3.0;
	/* and close up the file */
	fclose(fp);

	/*
	 ** Allocate memory.
	 **
	 ** Remember C is pass-by-value, so we need to
	 ** pass pointers into the initialise function.
	 **
	 ** NB we are allocating a 1D array, so that the
	 ** memory will be contiguous.  We still want to
	 ** index this memory as if it were a (row major
	 ** ordered) 2D array, however.  We will perform
	 ** some arithmetic using the row and column
	 ** coordinates, inside the square brackets, when
	 ** we want to access elements of this array.
	 **
	 ** Note also that we are using a structure to
	 ** hold an array of 'speeds'.  We will allocate
	 ** a 1D array of these structs.
	 */

	/* main grid */
	*cells_ptr = (t_speed*) aligned_malloc(sizeof(t_speed) * (params->ny * params->nx));
	if (*cells_ptr == NULL)
		die("cannot allocate memory for cells", __LINE__, __FILE__);

	*cells_0 = (float*) aligned_malloc(sizeof(float) * (params->ny * params->nx));
	if (*cells_0 == NULL)
		die("cannot allocate memory for cells0", __LINE__, __FILE__);

	/* 'helper' grid, used as scratch space */
	*tmp_cells_ptr = (t_speed*) aligned_malloc(
			sizeof(t_speed) * (params->ny * params->nx));
	if (*tmp_cells_ptr == NULL)
		die("cannot allocate memory for tmp_cells", __LINE__, __FILE__);

	/* the map of obstacles */
	*obstacles_ptr = aligned_malloc(sizeof(char) * BITARRAY(params->ny * params->nx));
	if (*obstacles_ptr == NULL)
		die("cannot allocate column memory for obstacles", __LINE__, __FILE__);

	/* initialise densities */
	w0 = params->density * 1.0 / 3.0;
	w1 = params->density * 1.0 / 6.0;

	for (ii = 0; ii < params->ny; ii++) {
		for (jj = 0; jj < params->nx; jj++) {
			/* centre */
			//(*cells_ptr)[ii * params->nx + jj].speeds[0] = w0;
			(*cells_0)[ii * params->nx + jj] = w0;
			/* axis directions */
			(*cells_ptr)[ii * params->nx + jj].speeds[0] = w1;
			(*cells_ptr)[ii * params->nx + jj].speeds[1] = w1;
			(*cells_ptr)[ii * params->nx + jj].speeds[2] = w1;
			(*cells_ptr)[ii * params->nx + jj].speeds[3] = w1;
		}
	}

	/* first set all cells in obstacle array to zero */
	for (ii = 0; ii < params->ny; ii++) {
		for (jj = 0; jj < params->nx; jj++) {
			INITBIT((*obstacles_ptr), ii * params->nx + jj);
			//(*obstacles_ptr)[ii * params->nx + jj] = 0;
		}
	}

	/* open the obstacle data file */
	fp = fopen(OBSTACLEFILE, "r");
	if (fp == NULL) {
		die("could not open file obstacles", __LINE__, __FILE__);
	}

	/* read-in the blocked cells list */
	while ((retval = fscanf(fp, "%d %d %d\n", &xx, &yy, &blocked)) != EOF) {
		/* some checks */
		if (retval != 3)
			die("expected 3 values per line in obstacle file", __LINE__,
					__FILE__);
		if (xx < 0 || xx > params->nx - 1)
			die("obstacle x-coord out of range", __LINE__, __FILE__);
		if (yy < 0 || yy > params->ny - 1)
			die("obstacle y-coord out of range", __LINE__, __FILE__);
		if (blocked != 1)
			die("obstacle blocked value should be 1", __LINE__, __FILE__);
		/* assign to array */
		//(*obstacles_ptr)[yy * params->nx + xx] = blocked;
		INSERT((*obstacles_ptr), yy * params->nx + xx);
	}

	/* and close the file */
	fclose(fp);

	/*
	 ** allocate space to hold a record of the avarage velocities computed
	 ** at each timestep
	 */
	//*av_vels_ptr = (float*) aligned_malloc(sizeof(float) * params->maxIters);


	return EXIT_SUCCESS;
}

int finalise(const t_param* params, t_speed** cells_ptr,
		t_speed** tmp_cells_ptr, char** obstacles_ptr, FILE* av_vels_file_ptr,float** cells_0) {
	/*
	 ** free up allocated memory
	 */
	aligned_free(*cells_ptr);
	*cells_ptr = NULL;

	aligned_free(*tmp_cells_ptr);
	*tmp_cells_ptr = NULL;

	aligned_free(*obstacles_ptr);
	*obstacles_ptr = NULL;

	//aligned_free(*av_vels_ptr);
	//*av_vels_ptr = NULL;
	fclose(av_vels_file_ptr);

	aligned_free(*cells_0);
	*cells_0 = NULL;



	return EXIT_SUCCESS;
}

float av_velocity(const t_param params, t_speed* cells, const char* obstacles,float* cells_0) {
	int ii, kk; /* generic counters */
	int tot_cells = 0; /* no. of cells used in calculation */
	float local_density; /* total density in cell */
	float tot_u_x; /* accumulated x-components of velocity */

	/* initialise */
	tot_u_x = 0.0;

	/* loop over all non-blocked cells */
#pragma omp parallel for private(ii) reduction(+: tot_u_x, tot_cells)
	for (ii = 0; ii < params.ny * params.nx; ii++) {
		//for (jj = 0; jj < params.nx; jj++) {
			/* ignore occupied cells */
			if (!ISBITSET(obstacles,ii)) {
				/* local density total */
				local_density = 0.0;
				for (kk = 0; kk < NSPEEDS; kk++) {
					local_density += cells[ii].speeds[kk];
				}
				local_density += cells_0[ii];
				/* x-component of velocity */
				tot_u_x += (cells[ii].speeds[0]
						- cells[ii].speeds[2]) / local_density;
				/* increase counter of inspected cells */
				++tot_cells;
			}
		//}
	}

	return tot_u_x / (float) tot_cells;
}

float calc_reynolds(const t_param params, t_speed* cells, const char* obstacles,float* cells_0) {
	const float viscosity = 1.0 / 6.0 * (2.0 / params.omega - 1.0);

	return av_velocity(params, cells, obstacles,cells_0) * params.reynolds_dim
			/ viscosity;
}

float total_density(const t_param params, t_speed* cells,float* cells_0) {
	int ii, kk; /* generic counters */
	float total = 0.0; /* accumulator */

#pragma omp parallel for private(ii) reduction(+: total)
	for (ii = 0; ii < params.ny*params.nx; ii++) {
		//for (jj = 0; jj < params.nx; jj++) {
			for (kk = 0; kk < NSPEEDS; kk++) {
				total += cells[ii].speeds[kk];
			}
			total+= cells_0[ii];
		//}
	}

	return total;
}

int write_values(const t_param params, t_speed* cells, const char* obstacles,
		float* cells_0) {
	FILE* fp; /* file pointer */
	int ii, jj, kk; /* generic counters */
	const float c_sq = 1.0 / 3.0; /* sq. of speed of sound */
	float local_density; /* per grid cell sum of densities */
	float pressure; /* fluid pressure in grid cell */
	float u_x; /* x-component of velocity in grid cell */
	float u_y; /* y-component of velocity in grid cell */

	fp = fopen(FINALSTATEFILE, "w");
	if (fp == NULL) {
		die("could not open file output file", __LINE__, __FILE__);
	}

	for (ii = 0; ii < params.ny; ii++) {
		for (jj = 0; jj < params.nx; jj++) {
			/* an occupied cell */
			if (ISBITSET(obstacles,ii * params.nx + jj)) {
				u_x = u_y = 0.0;
				pressure = params.density * c_sq;
			}
			/* no obstacle */
			else {
				local_density = 0.0;
				for (kk = 0; kk < NSPEEDS; kk++) {
					local_density += cells[ii * params.nx + jj].speeds[kk];
				}
				local_density += cells_0[ii * params.nx +jj];
				/* compute x velocity component */
				u_x = (cells[ii * params.nx + jj].speeds[0]
						+ -cells[ii * params.nx + jj].speeds[2])
						/ local_density;
				/* compute y velocity component */
				u_y = (cells[ii * params.nx + jj].speeds[1]
						- cells[ii * params.nx + jj].speeds[3]) / local_density;
				/* compute pressure */
				pressure = local_density * c_sq;
			}
			/* write to file */
			fprintf(fp, "%d %d %.6f %.6f %.6f %d\n", ii, jj, u_x, u_y,
					pressure, obstacles[ii * params.nx + jj]);
		}
	}

	fclose(fp);

	//for (ii = 0; ii < params.maxIters; ii++) {
		//fprintf(fp, "%d:\t%.12E\n", ii, av_vels[ii]);
	//}

	//fclose(fp);

	return EXIT_SUCCESS;
}

void die(const char* message, const int line, const char *file) {
	fprintf(stderr, "Error at line %d of file %s:\n", line, file);
	fprintf(stderr, "%s\n", message);
	fflush(stderr);
	exit(EXIT_FAILURE);
}
/*
int timestep(const t_param params, t_speed* cells, t_speed* tmp_cells,
		const int* obstacles,float* vel) {
	//accelerate_flow(params, cells, obstacles);
	//propagate(params, cells, tmp_cells);
	//rebound(params, cells, tmp_cells, obstacles);
	together(params, cells, tmp_cells, obstacles);
	collision(params, cells, tmp_cells, obstacles,vel);
	return EXIT_SUCCESS;
}
*/
/*
int accelerate_flow(const t_param params, t_speed* cells, const int* obstacles) {
	int ii,cellIndex;
	//float w1;

	//w1 = params.density * params.accel / 3.0;

#pragma omp parallel for default(none) shared(cells,obstacles) private(ii,cellIndex) schedule(dynamic,50)
	for (ii = 0; ii < params.ny; ii++) {
		cellIndex = ii * params.nx;
		if (!obstacles[cellIndex]
				&& (cells[cellIndex].speeds[3] - params.weighting_factor) > 0.0) {
			cells[cellIndex].speeds[1] += params.weighting_factor;
			cells[cellIndex].speeds[3] -= params.weighting_factor;
		}
	}

	return EXIT_SUCCESS;
}
*/
/*
int propagate(const t_param params, t_speed* cells, t_speed* tmp_cells) {
	int ii, jj;

	int x_e, x_w, y_n, y_s;

	for (ii = 0; ii < params.ny; ii++) {
		for (jj = 0; jj < params.nx; jj++) {
			y_n = (ii + 1) % params.ny;
			x_e = (jj + 1) % params.nx;
			y_s = (ii == 0) ? (ii + params.ny - 1) : (ii - 1);
			x_w = (jj == 0) ? (jj + params.nx - 1) : (jj - 1);
			tmp_cells[ii * params.nx + jj].speeds[0] =
					cells[ii * params.nx + jj].speeds[0];
			tmp_cells[ii * params.nx + x_e].speeds[1] = cells[ii * params.nx
					+ jj].speeds[1];
			tmp_cells[y_n * params.nx + jj].speeds[2] = cells[ii * params.nx
					+ jj].speeds[2];
			tmp_cells[ii * params.nx + x_w].speeds[3] = cells[ii * params.nx
					+ jj].speeds[3];
			tmp_cells[y_s * params.nx + jj].speeds[4] = cells[ii * params.nx
					+ jj].speeds[4];
		}
	}

	return EXIT_SUCCESS;
}
*/
/*
int propagate(const t_param params, t_speed* cells, t_speed* tmp_cells) {
	int ii;
	const int ny = params.ny;
	const int nx = params.nx;
	const int ny_1nx = (ny - 1) * nx;
	const int nynx = ny * nx;
	const int nx_1 = nx - 1;
	int jj=0;
	//Loop1
#pragma omp parallel default(none) shared(tmp_cells,cells) private(ii) firstprivate(jj)
	{

#pragma omp for
	for (ii = 0; ii < nynx; ii++) { // All
		tmp_cells[ii].speeds[0] = cells[ii].speeds[0];
		tmp_cells[ii].speeds[1] = cells[ii - 1].speeds[1];
		tmp_cells[ii].speeds[3] = cells[ii + 1].speeds[3];
	}
#pragma omp for nowait
	for (ii = 0; ii < nynx; ii += nx) {
		tmp_cells[ii].speeds[1] = cells[ii + nx_1].speeds[1];
	}
	//Loop2- must be after loop1
	//Loop3 - must be after loop1
#pragma omp for nowait
	for (ii = nx - 1; ii < nynx; ii += nx) {
		tmp_cells[ii].speeds[3] = cells[ii - nx_1].speeds[3];
	}

	//Loop4 b - no sequence needed
#pragma omp for nowait
	for (ii = 0; ii < nx; ii++) { // All
		tmp_cells[ii].speeds[2] = cells[ny_1nx + ii].speeds[2];
	}
	//Loop4 a - no sequence needed
#pragma omp for nowait
	for (ii = nx; ii < nynx; ii++) {
		tmp_cells[ii].speeds[2] = cells[ii - nx].speeds[2];
	}

	//Loop5 a - no sequence needed
#pragma omp for nowait
	for (ii = 0; ii < ny_1nx; ii++) {
		tmp_cells[ii].speeds[4] = cells[ii + nx].speeds[4];
	}
	//Loop6 a - no sequence needed
#pragma omp for nowait
	for (ii = ny_1nx; ii < nynx; ii++) {
		tmp_cells[ii].speeds[4] = cells[jj].speeds[4];
		jj++;
	}

	}

	return EXIT_SUCCESS;

}*/
/*
int rebound(const t_param params, t_speed* cells, t_speed* tmp_cells,
		const int* obstacles) {
	int ii;
	const int nynx = params.ny*params.nx;
#pragma omp parallel for default(none) shared(cells,tmp_cells,obstacles) private(ii) schedule(dynamic,300)
	for (ii = 0; ii < nynx; ii++) {
		//for (jj = 0; jj < params.nx; jj++) {
			if (obstacles[ii]) {
				cells[ii].speeds[1] = tmp_cells[ii].speeds[3];
				cells[ii].speeds[2] = tmp_cells[ii].speeds[4];
				cells[ii].speeds[3] = tmp_cells[ii].speeds[1];
				cells[ii].speeds[4] = tmp_cells[ii].speeds[2];
			}
		//}
	}

	return EXIT_SUCCESS;
}
*/
/*
int collision(const t_param params, t_speed* cells, t_speed* tmp_cells,
		const int* obstacles,float* vel) {
	int ii, kk;
	const int nynx = params.ny * params.nx;
	float tot_u_x = 0;
	int tot_cells = 0;
#pragma omp parallel for default(none) shared(cells,tmp_cells,obstacles) private(ii,kk) reduction(+: tot_u_x,tot_cells) schedule(dynamic,300)
	for (ii = 0; ii < nynx; ii++) {
		float d_equ[NSPEEDS];
		float local_density;

		//for (jj = 0; jj < params.nx; jj++) {
		if (!obstacles[ii]) {
			local_density = 0.0;
			for (kk = 0; kk < NSPEEDS; kk++) {
				local_density += tmp_cells[ii].speeds[kk];
			}

			calc(tmp_cells[ii].speeds[1], tmp_cells[ii].speeds[2],
					tmp_cells[ii].speeds[3], tmp_cells[ii].speeds[4],
					local_density, d_equ);

			local_density = 0.0;
			for (kk = 0; kk < NSPEEDS; kk++) {
				cells[ii].speeds[kk] =
						(tmp_cells[ii].speeds[kk]
								+ params.omega
										* (d_equ[kk] - tmp_cells[ii].speeds[kk]));
				local_density += cells[ii].speeds[kk];
			}

			tot_u_x += (cells[ii].speeds[1]
			- cells[ii].speeds[3]) / local_density;
			tot_cells+=1;
		}
		//}
	}
	*vel= tot_u_x / (float) tot_cells;
	return EXIT_SUCCESS;
}
*/
