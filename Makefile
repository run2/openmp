# Makefile
# includes a test rule, see test.sh

EXE1=learnopenmp.exe
EXES=$(EXE1)

CC=gcc
# CFLAGS=-lm -Wall -DDEBUG
# link math library and print all warnings
CFLAGS=-lm -Wall
CFLAGS+=-O3
#only debug information for gdb maybe 
#CFLAGS+=-g
#information for gprof
#CFLAGS+=-pg
# trying to vectorize loops
#CFLAGS+=-O2
#CFLAGS+=-ftree-vectorize -msse2 -ftree-vectorizer-verbose=7
#CFLAGS+=-ftree-vectorizer-verbose=7
#CFLAGS+=-finline-functions
##CFLAGS+=-ffast-math -fno-signed-zeros
#CFLAGS+=-fprofile-use 
##CFLAGS+=-fstrict-aliasing 
#CFLAGS+=-fprefetch-loop-arrays
#CFLAGS+=-freorder-functions -freorder-blocks-and-partition -fprefetch-loop-arrays -ftree-loop-distribution
#CFLAGS+=-mtune=opteron -ftree-vectorize -funroll-loops -msse2 -mfpmath=sse 
#CFLAGS+=-malign-double
#CFLAGS+=-fmodulo-sched -fmodulo-sched-allow-regmoves
##CFLAGS+=-fpredictive-commoning
#CFLAGS+=-fsection-anchors
CFLAGS+=-mtune=opteron
CFLAGS+=-msse2
CFLAGS+=-fopenmp
#CFLAGS+=-std=c99

all: $(EXES)

$(EXES): %.exe : %.c
	$(CC) $(CFLAGS) $^ -o $@

.PHONY: all clean  test

clean:
	\rm -f $(EXES)

test: $(EXES)
	./test.sh
